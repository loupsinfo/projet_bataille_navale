#ifndef CUIRASSE_H
#define CUIRASSE_H


class Cuirasse : public Navire
{
    public:
        Cuirasse(int pType, int pPositionX, int pPositionY, int pOrientation);
        virtual ~Cuirasse();
    protected:
    private:
};

#endif /// CUIRASSE_H
