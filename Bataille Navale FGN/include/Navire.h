#ifndef NAVIRE_H
#define NAVIRE_H


class Navire
{
    public:
        Navire(int pType, int pPos_x, int pPos_Y);
        virtual ~Navire();

        int GetType() {return m_type}
        void SetType (int x) {m_type=x;}
        int getTaille() {return m_taille;}
        void SetTaille (int x) {m_taille=x;}
        int GetPos_x() {return m_posx;}
        void SetPos_x(int x) {m_posx=x;}
        int GetPos_y() {return m_posy;}
        void SetPos_y(int x) {m_posy=x;}
        int GetPDF(){return m_pdf;}
        void GetPDF(int m_pdf) {m_pdf=x;}


    protected:
        string m_nom;
        int m_type;
        int m_taille;
        int m_posx;
        int m_posy;
        int m_pdf;

    private:
};

#endif /// NAVIRE_H
