#ifndef PLATEAU_H
#define PLATEAU_H


class Plateau
{
    public:
        void initialiser_plateau();
        void afficher_plateau();

        Plateau();
        virtual ~Plateau();
        int Getlig() { return lig; }
        void Setlig(int val) { lig = val; }
        int Getcol() { return col; }
        void Setcol(int val) { col = val; }

    protected:
    private:
        int lig;
        int col;
        int m_numcol;
        char m_lettrelig;

};

#endif // PLATEAU_H
