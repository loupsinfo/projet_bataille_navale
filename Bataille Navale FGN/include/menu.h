#ifndef MENU_H
#define MENU_H


class menu
{
    public:
        menu();
        virtual ~menu();
        void Afficher_Menu();
        void Afficher_Aide();
        void Charger_Partie();
        void Quitter();
    protected:

    private:
};

#endif /// MENU_H
