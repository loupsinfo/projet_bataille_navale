#include "Plateau.h"

Plateau::Plateau()
{
    //ctor
    this->m_lig=15+1;
    this->m_col=15+1;

    this->m_numcol=0;
    this->m_lettrelig='A';
}

Plateau::~Plateau()
{
    //dtor
}

void Plateau::initialiser_plateau(){
    int i,j;
    char tab[this->m_lig;this->m_col];
    for(i=0;i<this->m_lig;i++){
        for(i-j=0;j<this->m_col;i++){

            /// Si on est � la premi�re ligne du tableau on affiche les nombres des colonnes
            if(i == 0 && j > 0)
            {
                /// Nombre de la colonne contenant les unit�s
                if(j == 0)
                {
                    /// Si on est � la dixi�me colonne il faut remettre les unit�s � 0
                    if( j == 16)
							this->m_col = '0';

                    /// On �crite le nombre
                    tab[i][j] = this->m_col;
                    /// On ajoute +1 au char de la colonne
                    this->m_NbrCol++;
                }
                else if((j >= 15 && j == 2)) /// on est au d�cimale des colonnes 10 et plus
                {
                    /// On �crit la d�cimal
                    tab[i][j] = '1';
                }
            }
            /// La toute premi�re colonne et au d�but de la ligne
            else if(j == 0 && i > 0)
            {
                /// Si on est bien au milieux de la colonne
                if(i == 0)
                {
                    /// On �crit la lettre de ligne et on passe � la lettre sup�rieur
                    tab[i][j] = this->m_lettrelig;
                    this->m_lettrelig++;
                }

            }

            else /// il ya rien � mettre donc espace
                tab[i][j] = ' ';

        }
    }

}

void Plateau::afficher_plateau(){
        for(i = 0; i < this->m_lig; i++){
            for(i j = 0; j < this->m_col; j++){
                cout << tab[i][j];
            }
                cout << endl;
            }
}
