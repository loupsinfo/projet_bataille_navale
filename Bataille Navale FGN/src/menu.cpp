#include "menu.h"

menu::menu()
{
    ///ctor
}

menu::~menu()
{
    ///dtor
}


void menu::Afficher_Menu()
{
    int choice;
    cout<<endl<<" ----------------------- MENU -----------------------"<<endl;
    cout <<endl<< "                  1. Debut du jeu                    " << endl;
    cout << "                  2. Charger une partie               " << endl;
    cout << "                  3. Aide                    " << endl;
    cout << "                  4. Quitter le jeu                   " << endl;
    cout << endl<<" ----------------------------------------------------" << endl;
    ///entrer la valeur
    cin >> choice;

    switch(choice)
    {
        case 1:
        begin_game();
        break;

        case 2:
        Charger_Partie();
        break;

        case 3:
        Afficher_Aide();
        break;

        case 4:
        Quitter();
        break;

        default:
        ///you have entered wrong menu item
        system("cls");    ///clear screen
        Afficher_Menu();        ///menu again
    }

    system("pause");
    return;
}

void menu::Afficher_Aide()
{
    system("cls");
    cout<<"Affichage:"<<endl;
    cout<<"Chaque possede deux grilles, l'une avec la position de ses bateaux et"<<endl;
    cout<<"la deuxieme pour voir les bateaux ennemis touches"<<endl;
    cout<<endl<<"Choix a chaque tour:"<<endl;
    cout<<"vous devez choisir un bateau et ensuite trois options s'offrent a vous:"<<endl;
    cout<<"- deplacer le bateau d'une seule case"<<endl;
    cout<<"- tourner toutes les cases d'un navire d'un angle de 90 degres"<<endl;
    cout<<"- tirer"<<endl;
    cout<<endl<<"But:"<<endl;
    cout<<"vous devez faire couler tous les bateaux ennemis"<<endl;
    cout<<endl<<"BONNE BATAILLE CAMARADE"<<endl;

    return;
}

void menu::Charger_Partie()
{
    return;
}

void menu::Quitter()
{
    return;
}

