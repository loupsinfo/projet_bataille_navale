#ifndef BATEAU_H
#define BATEAU_H

#include <string>

using namespace std;

class Bateau
{
    public:
        Bateau(int pType, int pPositionX, int pPositionY, int pOrientation);
        virtual ~Bateau();

        unsigned int GetType() { return m_Type; }
        void SetType(unsigned int val) { m_Type = val; }
        string GetNom() { return m_Nom; }
        void SetNom(string val) { m_Nom = val; }
        char GetSymbole() { return m_Symbole; }
        void SetSymbole(char val) { m_Symbole = val; }
        unsigned int GetTaille() { return m_Taille; }
        void SetTaille(unsigned int val) { m_Taille = val; }
        unsigned int GetPuissanceDeFeu() { return m_PuissanceDeFeu; }
        void SetPuissanceDeFeu(unsigned int val) { m_PuissanceDeFeu = val; }
        unsigned int GetPosX() { return m_PosX; }
        void SetPosX(unsigned int val) { m_PosX = val; }
        unsigned int GetPosY() { return m_PosY; }
        void SetPosY(unsigned int val) { m_PosY = val; }
        unsigned int GetOrientation() { return m_Orientation; }
        void SetOrientation(unsigned int val) { m_Orientation = val; }

    protected:
        unsigned int m_Type;
        string m_Nom;

        char m_Symbole;
        unsigned int m_Taille;
        unsigned int m_PuissanceDeFeu;
        unsigned int m_PosX;
        unsigned int m_PosY;
        unsigned int m_Orientation;

    private:
};

#endif // BATEAU_H
