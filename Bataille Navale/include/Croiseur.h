#ifndef CROISEUR_H
#define CROISEUR_H

#include <Bateau.h>


class Croiseur : public Bateau
{
    public:
        Croiseur(int pType, int pPositionX, int pPositionY, int pOrientation);
        virtual ~Croiseur();

    protected:

    private:
};

#endif /// CROISEUR_H
