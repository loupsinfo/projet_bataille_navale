#ifndef CUIRASSE_H
#define CUIRASSE_H

#include <Bateau.h>


class Cuirasse : public Bateau
{
    public:
        Cuirasse(int pType, int pPos_x, int pPos_y);
        virtual ~Cuirasse();

    protected:

    private:
};

#endif // CUIRASSE_H
