#ifndef DESTROYER_H
#define DESTROYER_H

#include <Bateau.h>


class Destroyer : public Bateau
{
    public:
        Destroyer(int pType, int pPositionX, int pPositionY, int pOrientation);
        virtual ~Destroyer();

        unsigned int GetNbrFlaire() { return m_NbrFlaire; }
        void SetNbrFlaire(unsigned int val) { m_NbrFlaire = val; }

    protected:

    private:
        unsigned int m_NbrFlaire;
};

#endif // DESTROYER_H
