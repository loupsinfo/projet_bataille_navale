#ifndef MATRICE_H
#define MATRICE_H

#include <iostream>
#include <vector>

using namespace std;

class Matrice
{
    public:
        Matrice();
        virtual ~Matrice();

        /// Fonction D'init de la matrice
        void InitMatrice();

        /// Affiche la matrice
        void DisplayMatrice();

        unsigned int GetTailleX() { return m_TailleX; }
        void SetTailleX(unsigned int val) { m_TailleX = val; }
        unsigned int GetTailleY() { return m_TailleY; }
        void SetTailleY(unsigned int val) { m_TailleY = val; }
        vector< vector<char> > GetMatrice() { return m_Matrice; }
        void SetMatrice(vector< vector<char> > val) { m_Matrice = val; }
        char GetAtPos(unsigned int PosX, unsigned int PosY);
        void SetOnPos(unsigned int PosX, unsigned int PosY, char c);

    protected:

    private:
        unsigned int m_TailleX;
        unsigned int m_TailleY;
        vector<vector<char> > m_Matrice;
        char m_LettreLine;
        char m_NbrCol;
};

#endif /// MATRICE_H
