#ifndef SOUSMARIN_H
#define SOUSMARIN_H

#include <Bateau.h>


class SousMarin : public Bateau
{
    public:
        SousMarin(int pType, int pPositionX, int pPositionY, int pOrientation);
        virtual ~SousMarin();

    protected:

    private:
};

#endif // SOUSMARIN_H
