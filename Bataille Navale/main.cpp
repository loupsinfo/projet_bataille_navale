#include <iostream>
#include <vector>
#include <cstdlib>

#include <Matrice.h>
#include <Bateau.h>
#include <Croiseur.h>

using namespace std;

void putShipOnGridRandomly(vector<Bateau*> navire, Matrice* mat);
void menu()
{
    cout<<endl<<" ----------------------- MENU -----------------------"<<endl;
    cout <<endl<< "                  1. Debut du jeu                    " << endl;
    cout << "                  2. Charger une partie               " << endl;
    cout << "                  3. Aide                    " << endl;
    cout << "                  4. Quitter le jeu                   " << endl;
    cout << endl<<" ----------------------------------------------------" << endl;
    return;
}

void begin_game()
{
    system("cls");
     Matrice* mat = new Matrice();

            vector<Bateau*> navire;

            /// Cr�e une liste de bateau avec une position de d�part � 0
            navire.push_back(new Croiseur(1, 0, 0, 0));
            navire.push_back(new Croiseur(1, 0, 0, 0));
            navire.push_back(new Croiseur(1, 0, 0, 0));
            navire.push_back(new Croiseur(1, 0, 0, 0));
            navire.push_back(new Croiseur(1, 0, 0, 0));

            putShipOnGridRandomly(navire, mat);

            mat->DisplayMatrice();
            return;
}

void Afficher_Regles()
{
    system("cls");
    cout<<"Affichage:"<<endl;
    cout<<"Chaque possede deux grilles, l'une avec la position de ses bateaux et"<<endl;
    cout<<"la deuxieme pour voir les bateaux ennemis touches"<<endl;
    cout<<endl<<"Choix a chaque tour:"<<endl;
    cout<<"vous devez choisir un bateau et ensuite trois options s'offrent a vous:"<<endl;
    cout<<"- deplacer le bateau d'une seule case"<<endl;
    cout<<"- tourner toutes les cases d'un navire d'un angle de 90 degres"<<endl;
    cout<<"- tirer"<<endl;
    cout<<endl<<"But:"<<endl;
    cout<<"vous devez faire couler tous les bateaux ennemis"<<endl;
    cout<<endl<<"BONNE BATAILLE CAMARADE"<<endl;

    return;
}

void Charger_Partie()
{
    return;
}

void Quitter()
{
    return;
}


int main()
{
    int choice;
	menu(); 	///montre le menu la premi�re fois
	///entrer la valeur
	cin >> choice;

	switch(choice)
	{
		case 1:
            begin_game();
			break;

		case 2:
			Charger_Partie();
			break;

		case 3:
			Afficher_Regles();
			break;

		case 4:
            Quitter();
            break;

		default:
			///you have entered wrong menu item
			system("cls");	///clear screen
			menu();		///menu again
	}

	system("pause");
	return 0;

}

/// Mettre les bateaux de mani�re al�atoire sur la matrice
void putShipOnGridRandomly(vector<Bateau*> navire, Matrice* mat)
{
    unsigned int posX;
    unsigned int posY;
    unsigned int orientation;
    bool isPlace = true;

    /// On parcourt toute la liste des bateaux
    for(unsigned int i = 0; i < navire.size(); i++)
    {

        do
        {
            /// Cr��e une position al�atoirement
            posX = rand() % 15;
            posY = rand() % 15;
            orientation = rand() % 2;

            /// Part du principe que la place est libre et on check si effectivement c'est le cas
            /// Si �a ne l'est pas on met isPLace en false et on recommence
            isPlace = true;

            /// Si l'emplacement de base est libre
            if(mat->GetAtPos(posX, posY) == ' ')
            {
                /// Si l'orienation est vertical et qu'on ne sort pas de la matrice du � la taille du navire
                if(orientation == 0)
                {
                    if((posX - 1 + navire[i]->GetTaille()) >= 15)
                        isPlace = false;
                    else
                    {
                        /// On check toutes les positions o� on va mettre le bateau
                        for(unsigned int j = posX; j < posX + navire[i]->GetTaille(); j++)
                        {
                            /// Si � un endroit on remarque qu'il n'y a pas d'emplacement vide
                            if(mat->GetAtPos(j, posY) != ' ')
                                isPlace = false;
                        }
                    }
                }
                /// Si l'orienation est horizontal et qu'on ne sort pas de la matrice du � la taille du navire
                if(orientation == 1)
                {
                    if((posY - 1 + navire[i]->GetTaille()) >= 15)
                        isPlace = false;
                    else
                    {
                        /// On check toutes les positions o� on va mettre le bateau
                        for(unsigned int j = posY; j < posY + navire[i]->GetTaille(); j++)
                        {
                            /// Si � un endroit on remarque qu'il n'y a pas d'emplacement vide
                            if(mat->GetAtPos(posX, j) != ' ')
                                isPlace = false;
                        }
                    }
                }
            }
            else
                isPlace = false;

        }while(isPlace == false);

        /// On a trouv� une position
        /// On met donc le bateau sur la grid
        if(orientation == 0)
        {
            for(unsigned int j = posX; j < (posX + navire[i]->GetTaille()); j++)
            {
                mat->SetOnPos(j, posY, navire[i]->GetSymbole());
            }
        }
        else
        {
            for(unsigned int j = posY; j < (posY + navire[i]->GetTaille()); j++)
            {
                mat->SetOnPos(posX, j, navire[i]->GetSymbole());
            }
        }

        navire[i]->SetOrientation(orientation);
        navire[i]->SetPosX(posX);
        navire[i]->SetPosY(posY);
    }
}
