#include "Croiseur.h"

Croiseur::Croiseur(int pType, int pPositionX, int pPositionY, int pOrientation) : Bateau(pType, pPositionX, pPositionY, pOrientation)
{
    this->m_Nom = "Croiseur";
    this->m_Taille = 5;
    this->m_PuissanceDeFeu = 4;
    this->m_Symbole = 'c';
}

Croiseur::~Croiseur()
{
    ///dtor
}
