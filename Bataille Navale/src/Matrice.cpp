#include "Matrice.h"

Matrice::Matrice()
{
    /// Init les tailles de la matrice
    this->m_TailleX = 15 * 2 + 2;
    this->m_TailleY = 15 * 3 + 2;

    /// Init les lignes et col de la matrice
    this->m_LettreLine = 'a';
    this->m_NbrCol = '0';

    /// Init la matrice compl�tement vide
    this->InitMatrice();
}

/// Fonction d'init de la matrice
void Matrice::InitMatrice()
{
    /// Parcourir tout le tableau
    for(unsigned int i = 0; i < this->m_TailleX; i++)
    {
        /// A chaque nouvelle ligne du tableau, cr��e le nombre de colonne n�cessaire
        this->m_Matrice.push_back(vector <char>(this->m_TailleY));

        for(unsigned int j = 0; j < this->m_TailleY; j++)
        {
            /// Si on est � la premi�re ligne du tableau on affiche les nombres des colonnes
            if(i == 0 && j > 0)
            {
                /// Nombre de la colonne contenant les unit�s
                if(j % 3 == 0)
                {
                    /// Si on est � la dixi�me colonne il faut remettre les unit�s � 0
                    if( j == 33)
							this->m_NbrCol = '0';

                    /// On �crite le nombre
                    this->m_Matrice[i][j] = this->m_NbrCol;
                    /// On ajoute +1 au char de la colonne
                    this->m_NbrCol++;
                }
                else if((j >= 32 && j % 3 == 2)) /// on est au d�cimale des colonnes 10 et plus
                {
                    /// On �crit la d�cimal
                    this->m_Matrice[i][j] = '1';
                }
            }
            /// La toute premi�re colonne et au d�but de la ligne
            else if(j == 0 && i > 0)
            {
                /// Si on est bien au milieux de la colonne
                if(i% 2 == 0)
                {
                    /// On �crit la lettre de ligne et on passe � la lettre sup�rieur
                    this->m_Matrice[i][j] = this->m_LettreLine;
                    this->m_LettreLine++;
                }
                else /// Sinon on �crit la s�paration des lignes
                {
                    this->m_Matrice[i][j] = '-';
                }
            }
            else if(j % 3 == 1)///Colonne qui marque la s�paration entre les colonnes
            {
                this->m_Matrice[i][j] = '|';
            }
            else if(i % 2 == 1 && j > 0) /// Ligne qui marque la s�paration
            {
                this->m_Matrice[i][j] = '-';
            }
            else /// il ya rien � mettre donc espace
                this->m_Matrice[i][j] = ' ';
        }
    }
}

void Matrice::DisplayMatrice()
{
    for(unsigned int i = 0; i < this->m_TailleX; i++)
    {
        for(unsigned int j = 0; j < this->m_TailleY; j++)
        {
            cout << this->m_Matrice[i][j];
        }

        cout << endl;
    }
}

/// Return a value at the position on the grid
char Matrice::GetAtPos(unsigned int PosX, unsigned int PosY)
{
    return(this->m_Matrice[PosX * 2 + 2][PosY * 2 + 2]);
}

void Matrice::SetOnPos(unsigned int PosX, unsigned int PosY, char c)
{
    this->m_Matrice[PosX * 2 + 2][PosY * 3 + 2] = c;
    this->m_Matrice[PosX * 2 + 2][PosY * 3 + 3] = c;
}

Matrice::~Matrice()
{
    ///dtor
}
